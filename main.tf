

# create skc security group  
resource "aws_security_group" "sg_skc_ssh" {
  name   = "sg_skc_ss"
  vpc_id = aws_vpc.skc_vpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg_skc_ssh"

  }

}

# creating a VPC
resource "aws_vpc" "skc_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "skc_vpc"
  }

}



# Creatomg a Subnet 
resource "aws_subnet" "skc_subnet" {
  vpc_id                  = aws_vpc.skc_vpc.id
  cidr_block              = var.subnet_cidr
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-2a"
  tags = {
    Name = "skc_subnet"
  }

}


# Creating a Internet Gateway 
resource "aws_internet_gateway" "skc_igw" {
  vpc_id = aws_vpc.skc_vpc.id
  tags = {
    Name = "skc_igw"
  }
}


// Create a route table 
resource "aws_route_table" "skc_public_rt" {
  vpc_id = aws_vpc.skc_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.skc_igw.id
  }
  tags = {
    Name = "skc_public_rt"
  }
}

// Associate subnet with routetable 

resource "aws_route_table_association" "skc_rta_public_subent_1" {
  subnet_id      = aws_subnet.skc_subnet.id
  route_table_id = aws_route_table.skc_public_rt.id

}