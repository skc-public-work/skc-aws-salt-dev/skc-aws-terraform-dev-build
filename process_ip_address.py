#!/usr/bin/env python

import sys
import os
import jinja2

#from  pythonClass.skcDoDynHostfileGitlab.dynGitlabIpClass import DynGitLabIPBuild
from skcPythonClass.skcTestClass import SkcTest
from skcPythonClass.skcDynamicIp import DynIpBuild
from skcPythonClass.skcParseOutput import ParseOutput





def main():
    print("Hello World!")
    print(sys.version)

    
    #dyip = ParseOutput()

    x = []
    #x=['private_ip_skc_minion_one = "10.10.1.207"', 'private_ip_skc_minion_three = "10.10.1.197"', 'private_ip_skc_minion_two = "10.10.1.27"', 'private_ip_skc_salt = "10.10.1.112"', 'public_ip_skc_minion_one = "3.145.60.222"', 'public_ip_skc_minion_three = "13.59.86.214"', 'public_ip_skc_minion_two = "3.22.42.149"', 'public_ip_skc_salt = "18.225.95.124"']

# use stdin if it's full
    if not sys.stdin.isatty():
        input_stream = sys.stdin

    # otherwise, read the given filename
    else:
        try:
            input_filename = sys.argv[1]
        except IndexError:
            message = 'need filename as first argument if stdin is not full'
            raise IndexError(message)
        else:
            input_stream = open(input_filename, 'rU')

    for line in input_stream:
        line = line.rstrip('\r\n')
        x.append(line) 

#     print(x)

    po = ParseOutput(x)

    # #po.printList()
    po.parseList()
    
    public_ip = po.returnThePublicDict()
    private_ip = po.returnThePrivateDict()

    print('stop here')

    dgl = DynIpBuild(public_ip, private_ip)

    dgl.printPrivateIp()
    dgl.printPublicIp()

    dgl.buildHostFileIps()

    print("end")

if __name__ == "__main__":
    main()