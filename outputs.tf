

output "public_ip_skc_salt" {
  description = "public_ip_skc_salt"
  value       = aws_instance.skc_salt.public_ip
}

output "private_ip_skc_salt" {
  description = "private_ip_skc_salt"
  value       = aws_instance.skc_salt.private_ip
}



output "public_ip_skc_minion_one" {
  description = "public_ip_skc_minion_one"
  value       = aws_instance.skc_minion_one.public_ip
}

output "private_ip_skc_minion_one" {
  description = "private_ip_skc_minion_one"
  value       = aws_instance.skc_minion_one.private_ip
}

output "public_ip_skc_minion_two" {
  description = "public_ip_skc_minion_two"
  value       = aws_instance.skc_minion_two.public_ip
}

output "private_ip_skc_minion_two" {
  description = "private_ip_skc_minion_two"
  value       = aws_instance.skc_minion_two.private_ip
}


output "public_ip_skc_minion_three" {
  description = "public_ip_skc_minion_three"
  value       = aws_instance.skc_minion_three.public_ip
}

output "private_ip_skc_minion_three" {
  description = "private_ip_skc_minion_three"
  value       = aws_instance.skc_minion_three.private_ip
}
