
import jinja2 



#============================================================
class DynIpBuild:
    '''
class to parse the terraform output data
    '''

    def __init__(self, publicIp, privateIp):

        print('initialize the DynIpBuild  gitlab ip build object')

        # self.templateLoader = jinja2.FileSystemLoader(searchpath="../../../ansible")
        # self.templateEnv = jinja2.Environment(loader=self.templateLoader)
        self.publicIpDic = publicIp
        self.privateIpDic = privateIp
        

    def printPublicIp(self):
        '''
        print the public ip
        '''
        print(self.publicIpDic)
        return
    
    def printPrivateIp(self):
        '''
        print the private ip
        '''
        print(self.privateIpDic)
        #print(self.privateIpDic['gitlab_main'])
        return

    def buildHostFileIps(self):
        '''
        populate the host_file_ips using the 
        host_file_ips.j2 template
        '''

        print('building the  host_file_ips file')
        
        #------------------------------------------------------------------
        templateLoader = jinja2.FileSystemLoader(searchpath="./ansible")
        templateEnv = jinja2.Environment(loader=templateLoader)
        TEMPLATE_FILE_hosts = "host_file_ips.j2"
        template_hosts = templateEnv.get_template(TEMPLATE_FILE_hosts)
        hostsOutput = template_hosts.render(salt_ip=self.privateIpDic['skc_salt'],
                                  minion_one_ip=self.privateIpDic['skc_minion_one'],
                                  minion_two_ip=self.privateIpDic['skc_minion_two'],
                                  minion_three_ip=self.privateIpDic['skc_minion_three']
                                         )
    
        fh = open("./ansible/vars/host_file_ips.yml", "w")
        fh.write(hostsOutput)
        fh.close()

        #-------------------------------------------------------------------


        #------------------------------------------------------------------
        templateLoader = jinja2.FileSystemLoader(searchpath="./ansible")
        templateEnv = jinja2.Environment(loader=templateLoader)
        TEMPLATE_FILE_hosts = "ssh.cfg.j2"
        template_hosts = templateEnv.get_template(TEMPLATE_FILE_hosts)
        hostsOutput = template_hosts.render(salt_ip_public=self.publicIpDic['skc_salt'],
                                minion_one_ip_public=self.publicIpDic['skc_minion_one'],
                                minion_two_ip_public=self.publicIpDic['skc_minion_two'],
                                minion_three_ip_public=self.publicIpDic['skc_minion_three']
                                         )
    
        fh = open("./ansible/ssh.cfg", "w")
        fh.write(hostsOutput)
        fh.close()

        #------------------------------------------------------------------
        

        #------------------------------------------------------------------

         
        
        #------------------------------------------------------------------
