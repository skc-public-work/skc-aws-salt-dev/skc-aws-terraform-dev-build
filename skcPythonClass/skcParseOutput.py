

import jinja2

#============================================================
class ParseOutput:
    '''
class to parse the terraform output data
    '''

    def __init__(self, listl):

        print('initialize the ParseOutput object')

        self.line_list = listl
        
        self.private_dict = {}
        self.public_dict = {}



    def printList(self):
        ''' 
         print the list passed in
        '''

        print(self.line_list)

        
    def parseList(self):
        '''
        parse the line and set appropriate flag, then collect ip
        '''
        print('working on separating public and private ip addresses')
        #print(self.line_list)

        for l in self.line_list:
            #print(l)
           
            if 'private' in l:
                #print(l)
                self.buildPrivateDict(l)
            elif 'public' in l:
                self.buildPublicDict(l)
            

    def buildPrivateDict(self, l):
        '''
        parse and add the line (l) to the private dictonary
        '''
        print('woking on adding data to private dictonary')
        #print(l)

        x = l.split('=')
        d = 2
        
        x0 = x[0].rstrip()
        x1 = x[1].lstrip()
        clean_x1 = x1[1:-1]
        n = x0.split('_')
        
        del n[0:d]
        #print(n)
        name = '_'.join(n)
        #print(name)
        self.private_dict[name] = clean_x1
        

    def printThePrivatgeDict(self):
        '''
        print the private ip dictionary
        '''
        print(self.private_dict)


    def buildPublicDict(self, l):
        '''
        parse and add line (l) data to the public dictionary
        '''
        d = 2
        x = l.split('=')
        x0 = x[0].rstrip()
        x1 = x[1].lstrip()
        clean_x1 = x1[1:-1]
        n = x0.split('_')
        del n[0:d]
        name = '_'.join(n)
        self.public_dict[name] = clean_x1

    def printThePublicDict(self):
        '''
        print the public ip dictionary
        '''
        print(self.public_dict)
    

    def returnThePublicDict(self):
        '''
        return the public dictionary
        '''
        return self.public_dict

    def returnThePrivateDict(self):
        '''
        return the private dictionary
        '''
        return self.private_dict

#============================================================

#=============================================================

def process_template(ip_data):

    print('in the process_template method')

    data_ranncher_ip = ip_data.rancher_ip.strip()
    data_ranncher_private_ip = ip_data.rancher_private_ip.strip()
    
    data_master_ip = ip_data.master_ip.strip()
    data_master_private_ip = ip_data.master_private_ip.strip()
    
    data_node_one_ip = ip_data.node_one_ip.strip()
    data_node_one_private_ip = ip_data.node_one_private_ip.strip()
    
    data_node_two_ip = ip_data.node_two_ip.strip()
    data_node_two_private_ip = ip_data.node_two_private_ip.strip()  
    
    # print(data_ranncher_ip)
    # print(data_ranncher_private_ip)
    # print(data_master_ip)
    # print(data_master_private_ip)
    # print(data_node_one_ip)
    # print(data_node_one_private_ip)
    # print(data_node_two_ip)
    # print(data_node_two_private_ip)

    
    
    templateLoader = jinja2.FileSystemLoader(searchpath="../ansible")
    templateEnv = jinja2.Environment(loader=templateLoader)
    TEMPLATE_FILE_ssh = "ssh.cfg.j2"
    template = templateEnv.get_template(TEMPLATE_FILE_ssh )
    outputText = template.render(rancher_ip=data_ranncher_ip,
                                 master_ip=data_master_ip,
                                 node_one_ip=data_node_one_ip,
                                 node_two_ip=data_node_two_ip
                                )

    
    f = open("../ansible/ssh.cfg", "w")
    f.write(outputText)
    f.close()

#------------------------------------------------------------------
    
    TEMPLATE_FILE_hosts = "host_file_ips.j2"
    template_hosts = templateEnv.get_template(TEMPLATE_FILE_hosts)
    hostsOutput = template_hosts.render(rancher_ip=data_ranncher_private_ip,
                                 master_ip=data_master_private_ip,
                                 node_one_ip=data_node_one_private_ip,
                                 node_two_ip=data_node_two_private_ip
                                         )
    
    fh = open("../ansible/vars/host_file_ips.yml", "w")
    fh.write(hostsOutput)
    fh.close()

#-------------------------------------------------------------------

    
    TEMPLATE_FILE_hosts = "skc-do_rancher.config.j2"
    template_hosts = templateEnv.get_template(TEMPLATE_FILE_hosts)
    hostsOutput = template_hosts.render(rancher_ip=data_ranncher_ip,
                                 master_ip=data_master_ip,
                                 node_one_ip=data_node_one_ip,
                                 node_two_ip=data_node_two_ip
                                         )
    
    fh = open("/home/centos/.ssh/config_files/do_rancher.config", "w")
    fh.write(hostsOutput)
    fh.close()


#============================================================
