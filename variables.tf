variable "ami" {
  default = "ami-0d965705421455fce"
}

variable "instance_type" {
  default = "t2.micro"
}


variable "vpc_cidr" {
  default = "10.10.0.0/16"
}

variable "subnet_cidr" {
  default = "10.10.1.0/24"
}