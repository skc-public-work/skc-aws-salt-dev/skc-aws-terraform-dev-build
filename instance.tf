




resource "aws_instance" "skc_salt" {
  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.sg_skc_ssh.id
  ]
  subnet_id = aws_subnet.skc_subnet.id

  tags = {
    Name = "skc_salt"
  }
}

resource "aws_instance" "skc_minion_one" {
  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.sg_skc_ssh.id
  ]
  subnet_id = aws_subnet.skc_subnet.id

  tags = {
    Name = "skc_minion_one"
  }
}


resource "aws_instance" "skc_minion_two" {
  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.sg_skc_ssh.id
  ]
  subnet_id = aws_subnet.skc_subnet.id

  tags = {
    Name = "skc_minion_two"
  }
}


resource "aws_instance" "skc_minion_three" {
  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.sg_skc_ssh.id
  ]
  subnet_id = aws_subnet.skc_subnet.id

  tags = {
    Name = "skc_minion_three"
  }
}




